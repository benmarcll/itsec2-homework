-- logging
SET serveroutput ON
 spool .\install.log

-- creation of users
@./users/itsec2_admin.sql
@./users/user1.sql
@./users/user2.sql
@./users/user3.sql
@./users/user4.sql

-- creation of tables
@./tables/user1.sql
@./tables/user2.sql
@./tables/user3.sql
@./tables/user4.sql
@./tables/student1.sql
@./tables/student4.sql

-- creation of procedures
@./procedures/del_graduated_students.sql
@./procedures/add_new_student.sql

-- creation of views
@./views/student_infos.sql


-- insert intos
@./inserts/user1.sql
@./inserts/user2.sql
@./inserts/user3.sql
@./inserts/user4.sql
@./inserts/student1.sql
@./inserts/student4.sql
commit;


 -- Recompile schema
BEGIN
  dbms_utility.compile_schema(schema => 'itsec2_admin');
END;
/
 
-- grants
@./grants/user1.sql
@./grants/user2.sql
@./grants/user3.sql
@./grants/user4.sql

-- turn of logging
spool off
