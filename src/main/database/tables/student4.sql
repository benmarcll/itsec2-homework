CREATE TABLE student4
(
   neptun_code	 	   VARCHAR2(6) PRIMARY KEY NOT NULL,
   first_name          VARCHAR2(50) NOT NULL,
   last_name           VARCHAR2(50) NOT NULL,
   mother_name         VARCHAR2(200) NOT NULL,
   email_address       VARCHAR2(100) UNIQUE NOT NULL,
   phone_number        VARCHAR2(20) UNIQUE NOT NULL,
   nationality         VARCHAR2(100) NOT NULL,
   password            VARCHAR2(200) NOT NULL,
   has_failed_in_math  NUMBER(1) NOT NULL,
   year_of_end         NUMBER(4) NOT NULL
);
