insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('ABC123','$2a$10$m5xH2p6sxkmLUvWMYy8rY.jLIZlGpNI/YvOCqAbEG3G/1/7YouufG','Siket','Zsuzsa','zsuzsy@gmal.com','+36301235678','Horváth Piroska','magyar',1,2018);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('QWE789','$2a$10$f4Zc88JAiG0QTb/20fZfP.N6wYjNKXuMzhdl8FeVp/sVoUbhiBtAS','Kakszi','Lajos','lali@tigris.hu','+36309854321','Balogh Éva','magyar',0,2016);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('POR544','$2a$10$5S.32ebiYVASfsusZnbmy.0yJxTVkq9HBQlWza0JNK5aL41WmPM6m','Kecskeméti','Ottó','impala@amerika.com','+36207432123','Kálloy Eszter','magyar',1,2023);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('PORRTT','$2a$10$epg/eJhE9UNOeFFuxr0kfu876u3K.2AE1A5rt4m0.p3JcqaaNNcg2','Zsíros','Ferenc','magnelkulidinnye@freemail.hu','+36702223451','Seress Bettina','magyar',0,2023);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('EEWWWE','$2a$10$7bdML9zHiAN5jD6zfabzeelVcb7hHoWZHIN/z/Uzi1J2DrlzCEZe6','Kántor','Géza','cingi@tuba.hu','+36308443234','Almási Julianna','magyar',0,2022);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('OMG12L','$2a$10$6BhPaaHGZUuhwevpkPnTRuYIUIC0OH1MweQbaDtuVOq6X.mqPfFm.','Turbók','Imre','izirajder@malibu.hu','+36204323456','Kerekes Mária','magyar',0,2023);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('QQEEER','$2a$10$nD4WS.OUmF5QgZ9gE.GIvu.lowtKXBUeASbZ3GlShQEcLRksbADxm','Király','Sándor','sanyiakiraly@nemadok.hu','+36308543288','Parti Orsolya','magyar',1,2023);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('MERREW','$2a$10$OPKxBOs4LyWjQQTNtMazc.nzJ3jQSYhGnNRJwJlhBVytmIereDeCe','Hodobay','Oszkár','palcsinta@trabant.hu','+36205654432','Molnár Teréz','magyar',0,2019);
insert into student1(neptun_code,password,last_name,first_name,email_address,phone_number,mother_name,nationality,has_failed_in_math,year_of_end)
values('AWQQEL','$2a$10$mjn9ctBDuiwpiUn2KvvCqufg.lLrcVgJeyJVjP9aQzD1OA5u57itW','Bajusz','Rezső','biztonsag@police.hu','+36307879843','Tóth Anna','magyar',1,2019);