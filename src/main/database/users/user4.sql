 -- Session-bol valo kileptetes, ha netalantan benne van 
DECLARE 
    CURSOR cur IS 
      SELECT 'alter system kill session ''' 
             || sid 
             || ',' 
             || serial# 
             || '''' AS command 
      FROM   v$session 
      WHERE  username = 'USER4'; 
BEGIN 
    FOR c IN cur LOOP 
        EXECUTE IMMEDIATE c.command; 
    END LOOP; 
END; 
/ 


-- user4 user, illetve a hozza kapcsolodo osszes objektum torlese 
DECLARE 
    v_count NUMBER; 
BEGIN 
    SELECT Count(*) 
    INTO   v_count 
    FROM   dba_users t 
    WHERE  t.username = 'USER4'; 

    IF v_count = 1 THEN 
      EXECUTE IMMEDIATE 'DROP USER user4 CASCADE'; 
    END IF; 
END; 
/ 

-- user4 user letrehozasa 
CREATE USER user4 IDENTIFIED BY "12345678" DEFAULT TABLESPACE users QUOTA 
UNLIMITED ON users; 