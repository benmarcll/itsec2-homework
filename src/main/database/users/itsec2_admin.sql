-- Session-bol valo kileptetes, ha netalantan benne van 
DECLARE 
    CURSOR cur IS 
      SELECT 'alter system kill session ''' 
             || sid 
             || ',' 
             || serial# 
             || '''' AS command 
      FROM   v$session 
      WHERE  username = 'ITSEC2_ADMIN'; 
BEGIN 
    FOR c IN cur LOOP 
        EXECUTE IMMEDIATE c.command; 
    END LOOP; 
END; 
/ 


-- itsec2_admin user, illetve a hozza kapcsolodo osszes objektum torlese 
DECLARE
    v_count NUMBER; 
BEGIN 
    SELECT Count(*) 
    INTO   v_count 
    FROM   dba_users t 
    WHERE  t.username = 'ITSEC2_ADMIN'; 

    IF v_count = 1 THEN 
      EXECUTE IMMEDIATE 'DROP USER itsec2_admin CASCADE'; 
    END IF; 
END; 
/ 


-- itsec2_admin user letrehozasa 
CREATE USER itsec2_admin IDENTIFIED BY "12345678" DEFAULT TABLESPACE users QUOTA 
UNLIMITED ON users; 



-- szukseges jogosultsagok megadasa 
GRANT CREATE SESSION TO itsec2_admin; 
GRANT CREATE TABLE TO itsec2_admin; 
GRANT CREATE VIEW TO itsec2_admin; 
GRANT CREATE SEQUENCE TO itsec2_admin; 
GRANT CREATE PROCEDURE TO itsec2_admin; 
GRANT CREATE TYPE TO itsec2_admin; 
GRANT CREATE TRIGGER TO itsec2_admin; 



-- Sema beallitasa a itsec2_admin-era 
ALTER SESSION SET current_schema=itsec2_admin; 



PROMPT done. 