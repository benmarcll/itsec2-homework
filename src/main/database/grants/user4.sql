GRANT CREATE SESSION TO user4; 

grant select on itsec2_admin.user4 to user4;
grant select on itsec2_admin.student_infos to user4;
grant execute on itsec2_admin.del_graduated_students to user4;
grant execute on itsec2_admin.add_new_student to user4;