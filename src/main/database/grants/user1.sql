GRANT CREATE SESSION TO user1;

grant select on itsec2_admin.user1 to user1;
grant select on itsec2_admin.student1 to user1;
grant delete on itsec2_admin.student1 to user1;
grant insert on itsec2_admin.student1 to user1;