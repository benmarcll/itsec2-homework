CREATE OR REPLACE PROCEDURE add_new_student(p_neptun_code        IN VARCHAR2
                                           ,p_first_name         IN VARCHAR2
                                           ,p_last_name          IN VARCHAR2
                                           ,p_mother_name        IN VARCHAR2
                                           ,p_email_address      IN VARCHAR2
                                           ,p_phone_number       IN VARCHAR2
                                           ,p_nationality        IN VARCHAR2
                                           ,p_has_failed_in_math IN VARCHAR2
                                           ,p_password           IN VARCHAR2
                                           ,p_year_of_end        IN VARCHAR2) IS

  neptun_code_format_exc  EXCEPTION;
  email_format_exc        EXCEPTION;
  phone_number_format_exc EXCEPTION;
  has_failed_in_math_exc  EXCEPTION;
  year_of_end_exc         EXCEPTION;
  all_field_necessary_exc EXCEPTION;
  neptun_not_unique       EXCEPTION;
  email_not_unique        EXCEPTION;
  phone_not_unique        EXCEPTION;

  PRAGMA EXCEPTION_INIT(neptun_code_format_exc, -20000);
  PRAGMA EXCEPTION_INIT(email_format_exc, -20001);
  PRAGMA EXCEPTION_INIT(phone_number_format_exc, -20002);
  PRAGMA EXCEPTION_INIT(has_failed_in_math_exc, -20003);
  PRAGMA EXCEPTION_INIT(year_of_end_exc, -20004);
  PRAGMA EXCEPTION_INIT(all_field_necessary_exc, -20005);
  PRAGMA EXCEPTION_INIT(neptun_not_unique, -20006);
  PRAGMA EXCEPTION_INIT(email_not_unique, -20007);
  PRAGMA EXCEPTION_INIT(phone_not_unique, -20008);

  v_neptun_code      VARCHAR2(2000);
  v_email_address    VARCHAR2(2000);
  v_neptun_code_uq   NUMBER;
  v_email_address_uq NUMBER;
  v_phone_number_uq  NUMBER;

BEGIN
  v_neptun_code   := upper(p_neptun_code);
  v_email_address := lower(p_email_address);

  IF v_neptun_code IS NULL
     OR p_first_name IS NULL
     OR p_last_name IS NULL
     OR p_mother_name IS NULL
     OR v_email_address IS NULL
     OR p_phone_number IS NULL
     OR p_nationality IS NULL
     OR p_password IS NULL
     OR p_has_failed_in_math IS NULL
     OR p_year_of_end IS NULL
  THEN
    RAISE all_field_necessary_exc;
  END IF;

  IF regexp_like(v_neptun_code, '^([A-Z0-9]){6}$')
  THEN
    NULL;
  ELSE
    RAISE neptun_code_format_exc;
  END IF;

  IF regexp_like(v_email_address, '^(\w|-|\.)+@((\w|-)+\.)+(\w|\-){2,4}$')
  THEN
    NULL;
  ELSE
    RAISE email_format_exc;
  END IF;

  IF regexp_like(p_phone_number, '^\+[0-9]{9,13}$')
  THEN
    NULL;
  ELSE
    RAISE phone_number_format_exc;
  END IF;

  IF regexp_like(p_has_failed_in_math, '^[0-1]$')
  THEN
    NULL;
  ELSE
    RAISE has_failed_in_math_exc;
  END IF;

  IF (regexp_like(p_year_of_end, '^\d{4}$') AND p_year_of_end BETWEEN 1900 AND 2030)
  THEN
    NULL;
  ELSE
    RAISE year_of_end_exc;
  END IF;

  SELECT COUNT(*)
    INTO v_neptun_code_uq
    FROM student4
   WHERE student4.neptun_code = v_neptun_code;

  IF v_neptun_code_uq > 0
  THEN
    RAISE neptun_not_unique;
  END IF;

  SELECT COUNT(*)
    INTO v_email_address_uq
    FROM student4
   WHERE student4.email_address = v_email_address;

  IF v_email_address_uq > 0
  THEN
    RAISE email_not_unique;
  END IF;

  SELECT COUNT(*)
    INTO v_phone_number_uq
    FROM student4
   WHERE student4.phone_number = p_phone_number;

  IF v_phone_number_uq > 0
  THEN
    RAISE phone_not_unique;
  END IF;

  INSERT INTO student4
    (neptun_code
    ,first_name
    ,last_name
    ,mother_name
    ,email_address
    ,phone_number
    ,nationality
    ,password
    ,has_failed_in_math
    ,year_of_end)
  VALUES
    (v_neptun_code
    ,p_first_name
    ,p_last_name
    ,p_mother_name
    ,v_email_address
    ,p_phone_number
    ,p_nationality
    ,p_password
    ,p_has_failed_in_math
    ,p_year_of_end);

END add_new_student;
/