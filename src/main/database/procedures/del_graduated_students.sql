CREATE OR REPLACE PROCEDURE del_graduated_students IS

  v_current_year NUMBER;

BEGIN

  SELECT extract(YEAR FROM SYSDATE) AS YEAR INTO v_current_year FROM dual;

  DELETE FROM student4 WHERE student4.year_of_end <= v_current_year;

END;
/