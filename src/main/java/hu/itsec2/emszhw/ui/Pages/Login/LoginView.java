package hu.itsec2.emszhw.ui.Pages.Login;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinSession;
import hu.itsec2.emszhw.model.User;
import hu.itsec2.emszhw.services.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;


@Route("login")
public class LoginView extends VerticalLayout {

    // SERVICES
    private final User1Services user1Services;
    private final User2Services user2Services;
    private final User3Services user3Services;
    private final User4Services user4Services;
    private final HttpService httpService = new HttpService();

    // LOGIN VIEW FIELDS
    private final Label loggedInUserLabel = new Label("Logged in user: -");
    private final Button logoutButton = new Button("Logout");
    private final Button treasureButton = new Button("View treasure!");
    private final HeaderComponent headerComponent = new HeaderComponent();
    private final HorizontalLayout contentComponent = new HorizontalLayout();
    private final HorizontalLayout buttonsLayout = new HorizontalLayout();
    private final VerticalLayout loggedInUserLayout = new VerticalLayout();
    private final HashMap<Tab, Component> tabToContentMap = new HashMap<>();


    // CONTENT FIELDS
    private final HorizontalLayout login1Layout = new HorizontalLayout();
    private final HorizontalLayout login2Layout = new HorizontalLayout();
    private final HorizontalLayout login3Layout = new HorizontalLayout();
    private final HorizontalLayout login4Layout = new HorizontalLayout();

    private final FormLayout login3FormLayout = new FormLayout();
    private final TextField login3Username = new TextField();
    private final PasswordField login3Password = new PasswordField();
    private final Button login3Button = new Button("Login");

    private final FormLayout login4FormLayout = new FormLayout();
    private final TextField login4Username = new TextField();
    private final PasswordField login4Password = new PasswordField();
    private final Button login4Button = new Button("Login");


    public LoginView(User1Services user1Services, User2Services user2Services, User3Services user3Services, User4Services user4Services) {

        // Autowired variables
        this.user1Services = user1Services;
        this.user2Services = user2Services;
        this.user3Services = user3Services;
        this.user4Services = user4Services;

        // Add to buttonsLayout Logout and View treasure! buttons
        buttonsLayout.add(logoutButton, treasureButton);

        // Add buttons and login label to its Wrapper
        loggedInUserLayout.add(loggedInUserLabel, buttonsLayout);

        // Init content component
        initContentComponent();

        // add header, loggedInUserLayout, content component
        this.add(headerComponent);
        this.add(loggedInUserLayout);
        this.add(contentComponent);

        // set up Logout, View treasure! buttons
        setUpButtonActions();

        // set up tab -> content mapping
        setUpMapping();
    }

    @PostConstruct
    public void afterConstruct() {

        // Make logout button disabled by default
        logoutButton.setEnabled(false);

        // If something is in the request's content
        if (VaadinRequest.getCurrent().getContentLength() > 0) {

            User loggedInUser;

            // switch case to handle which form sent the request
            switch (VaadinRequest.getCurrent().getParameter("login_form_id")) {
                case "login1" -> loggedInUser = user1Services.logIn(
                        VaadinRequest.getCurrent().getParameter("username"),
                        VaadinRequest.getCurrent().getParameter("password")
                );
                case "login2" -> loggedInUser = user2Services.logIn(
                        VaadinRequest.getCurrent().getParameter("username"),
                        VaadinRequest.getCurrent().getParameter("password")
                );
                default -> loggedInUser = null;
            }

            // If it was a valid login
            if (loggedInUser != null) {
                logInLogOut(loggedInUser);
            } else {
                Notification.show("Login failed!");
            }

            // A simple navigate , to make sure that we can't re-submit the login
            // request by using "back" function of the browser
            UI.getCurrent().navigate("login");

        } else {

            // If it wasnt a request that contains content, then it was a simple reload, so login with that user who was logged in.
            // It could be even a null user
            logInLogOut((User) VaadinSession.getCurrent().getAttribute("loggedInUser"));
        }

    }

    private void setUpButtonActions() {
        logoutButton.addClickListener(click -> {
            Notification.show("Logout...");
            logInLogOut(null);
        });

        treasureButton.addClickListener(click -> UI.getCurrent().getPage().setLocation("treasure"));
    }

    private void setUpMapping() {
        // for loop to loop over content pages (login1,login2...)
        for (int i = 0; i < contentComponent.getChildren().count(); i++) {

            // and put to a map tab[i] -> page[i]
            tabToContentMap.put(
                    headerComponent.getTabs().getChildren()
                            .map(child -> (Tab) child)
                            .toArray(
                                    Tab[]::new)[i],
                    contentComponent.getChildren().toArray(Component[]::new)[i]
            );
        }

        headerComponent.getTabs().addSelectedChangeListener(selectedChangeEvent -> {
            if (VaadinSession.getCurrent().getAttribute("loggedInUser") == null) {
                tabToContentMap.values().forEach(page -> page.setVisible(false));
                tabToContentMap.get(headerComponent.getTabs().getSelectedTab()).setVisible(true);
            }
        });
    }

    private void logInLogOut(User loggedInUser) {

        // If the parameter is null, then its a logout
        if (loggedInUser != null) {

            tabToContentMap.get(headerComponent.getTabs().getSelectedTab()).setVisible(false);
            logoutButton.setEnabled(true);
            loggedInUserLabel.setText("Logged in user: " + loggedInUser.getUsername());
            VaadinSession.getCurrent().setAttribute("loggedInUser", loggedInUser);

        } else {

            tabToContentMap.get(headerComponent.getTabs().getSelectedTab()).setVisible(true);
            logoutButton.setEnabled(false);
            loggedInUserLabel.setText("Logged in user: -");
            VaadinSession.getCurrent().setAttribute("loggedInUser", null);

        }
    }

    private void initContentComponent() {
        contentComponent.setWidthFull();
        contentComponent.setJustifyContentMode(JustifyContentMode.CENTER);

        // Init login pages
        setUpLogin1Layout(httpService.getLogin1);
        setUpLogin2Layout(httpService.getLogin2);
        setUpLogin3Layout();
        setUpLogin4Layout();

        contentComponent.add(login1Layout, login2Layout, login3Layout, login4Layout);
    }

    private void setUpLogin1Layout(String login1HtmlContent) {
        login1Layout.getElement().setProperty("margin-top", "30px");

        // Set its inner html an actual html code
        login1Layout.getElement().setProperty("innerHTML", login1HtmlContent);
        login1Layout.setVisible(true);
    }

    private void setUpLogin2Layout(String login2HtmlContent) {
        login2Layout.getElement().setProperty("margin-top", "30px");

        // Set its inner html an actual html code
        login2Layout.getElement().setProperty("innerHTML", login2HtmlContent);
        login2Layout.setVisible(false);

        login2Layout.add(login2HtmlContent);
    }

    private void setUpLogin3Layout() {
        login3Layout.setWidth("325px");

        login3Username.setRequired(true);
        login3Password.setRequired(true);

        login3Layout.getElement().getStyle().set("margin-top", "30px");
        login3Layout.setVisible(false);

        // force the elements be vertical
        login3FormLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("25em", 1));

        login3FormLayout.add(new H1("Login3"));
        login3FormLayout.addFormItem(login3Username, "Username");
        login3FormLayout.addFormItem(login3Password, "Password");
        login3FormLayout.addFormItem(login3Button, "");

        login3Button.addClickListener(listener -> {

            // user will be null or a valid user
            User loggedInUser = user3Services.logIn(login3Username.getValue(), login3Password.getValue());

            if (loggedInUser == null) {
                Notification.show("Login failed!");
            }

            // clear both inputs
            login3Username.clear();
            login3Password.clear();

            // and log in with the user (null or valid still)
            logInLogOut(loggedInUser);

        });

        login3Layout.add(login3FormLayout);
    }

    private void setUpLogin4Layout() {
        login4Layout.setWidth("325px");

        login4Username.setRequired(true);
        login4Password.setRequired(true);

        login4Layout.getElement().getStyle().set("margin-top", "30px");
        login4Layout.setVisible(false);

        // force the elements be vertical
        login4FormLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("25em", 1));

        login4FormLayout.add(new H1("Login4"));
        login4FormLayout.addFormItem(login4Username, "Username");
        login4FormLayout.addFormItem(login4Password, "Password");
        login4FormLayout.addFormItem(login4Button, "");

        login4Button.addClickListener(listener -> {

            // user will be null or a valid user
            User loggedInUser = user4Services.logIn(login4Username.getValue(), login4Password.getValue());

            if (loggedInUser == null) {
                Notification.show("Login failed!");
            }

            // clear both inputs
            login4Username.clear();
            login4Password.clear();

            // and log in with the user (null or valid still)
            logInLogOut(loggedInUser);

        });

        login4Layout.add(login4FormLayout);
    }
}
