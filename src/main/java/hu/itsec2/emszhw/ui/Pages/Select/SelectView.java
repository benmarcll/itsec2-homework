package hu.itsec2.emszhw.ui.Pages.Select;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import hu.itsec2.emszhw.services.User1Services;
import hu.itsec2.emszhw.services.User4Services;
import hu.itsec2.emszhw.ui.CustomComponents.Grids.Student1Grid;
import hu.itsec2.emszhw.ui.CustomComponents.Grids.Student4Grid;

import javax.annotation.PostConstruct;

@Route("select")
public class SelectView extends VerticalLayout {
    private final User1Services user1Services;
    private final User4Services user4Services;

    private final VerticalLayout selectLayout = new VerticalLayout();
    private final VerticalLayout viewLayout = new VerticalLayout();

    private final Student1Grid selectGrid = new Student1Grid();
    private final Student4Grid viewGrid = new Student4Grid();

    public SelectView(User1Services user1Services, User4Services user4Services) {
        this.user1Services = user1Services;
        this.user4Services = user4Services;

        selectGrid.setDataProvider(DataProvider.ofCollection(user1Services.getStudentsData()));
        viewGrid.setDataProvider(DataProvider.ofCollection(user4Services.getStudentsData()));

        setSelectLayout();
        setViewLayout();

        this.setHeight("100%");

        this.add(selectLayout, viewLayout);
    }

    @PostConstruct
    public void afterConstruct() {
    }

    private void setSelectLayout() {
        selectGrid.setDataProvider(DataProvider.ofCollection(user1Services.getStudentsData()));

        H1 h1 = new H1("SELECT");
        h1.getElement().getStyle().set("margin-top", "10px");
        h1.getElement().getStyle().set("margin-bottom", "10px");

        selectLayout.add(h1);
        selectLayout.setWidth("100%");
        selectLayout.setHeight("50%");
        selectLayout.setAlignItems(Alignment.CENTER);

        selectLayout.add(selectGrid);
    }

    private void setViewLayout() {
        H1 h1 = new H1("VIEW");
        h1.getElement().getStyle().set("margin-top", "10px");
        h1.getElement().getStyle().set("margin-bottom", "10px");
        viewLayout.add(h1);
        viewLayout.setWidth("100%");
        viewLayout.setHeight("50%");
        viewLayout.setAlignItems(Alignment.CENTER);

        viewLayout.add(viewGrid);
    }


}
