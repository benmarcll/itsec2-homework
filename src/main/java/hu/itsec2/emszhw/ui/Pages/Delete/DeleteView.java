package hu.itsec2.emszhw.ui.Pages.Delete;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import hu.itsec2.emszhw.model.Student;
import hu.itsec2.emszhw.services.User1Services;
import hu.itsec2.emszhw.services.User4Services;
import hu.itsec2.emszhw.ui.CustomComponents.Grids.Student1Grid;
import hu.itsec2.emszhw.ui.CustomComponents.Grids.Student4Grid;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Route("delete")
public class DeleteView extends VerticalLayout {
    private final User1Services user1Services;
    private final User4Services user4Services;

    private final VerticalLayout deleteLayout = new VerticalLayout();
    private final VerticalLayout procedureLayout = new VerticalLayout();

    private final Student1Grid deleteGrid = new Student1Grid();
    private final Student4Grid procedureGrid = new Student4Grid();

    public DeleteView(User1Services user1Services, User4Services user4Services) {
        this.user1Services = user1Services;
        this.user4Services = user4Services;

        refreshGridData(deleteGrid, user1Services.getStudentsData());
        refreshGridData(procedureGrid, user4Services.getStudentsData());

        setDeleteLayout();
        setProcedureLayout();

        this.setHeight("100%");

        this.add(deleteLayout, procedureLayout);
    }

    private void refreshGridData(Grid<Student> grid, List<Student> studentsData) {
        grid.setDataProvider(DataProvider.ofCollection(studentsData));
    }

    @PostConstruct
    public void afterConstruct() {
    }

    private void setDeleteLayout() {
        H1 h1 = new H1("Delete by SQL");
        h1.getElement().getStyle().set("margin-top", "10px");
        h1.getElement().getStyle().set("margin-bottom", "10px");

        deleteGrid.setSelectionMode(Grid.SelectionMode.MULTI);

        Button deleteButton = new Button("Delete selected ones");
        deleteButton.addClickListener(click -> {
            if (user1Services.deleteStudents(deleteGrid.getSelectedItems().stream().map(Student::getNeptunCode).collect(Collectors.toSet()))) {
                refreshGridData(deleteGrid, user1Services.getStudentsData());
            }
        });

        deleteLayout.setWidth("100%");
        deleteLayout.setHeight("50%");
        deleteLayout.setAlignItems(Alignment.CENTER);

        deleteLayout.add(h1);
        deleteLayout.add(deleteGrid);
        deleteLayout.add(deleteButton);
    }

    private void setProcedureLayout() {
        H1 h1 = new H1("Delete by procedure");
        h1.getElement().getStyle().set("margin-top", "10px");
        h1.getElement().getStyle().set("margin-bottom", "10px");

        Button deleteButton = new Button("Delete only gratuated students");
        deleteButton.addClickListener(click -> {
            if (user4Services.deleteGraduatedStudents()) {
                refreshGridData(procedureGrid, user4Services.getStudentsData());
            }
        });

        procedureGrid.removeAllColumns();
        procedureGrid.addColumn(Student::getNeptunCode).setHeader("Neptun code").setSortable(true).setAutoWidth(true);
        procedureGrid.addColumn(Student::getYearOfEnd).setHeader("Year Of End").setSortable(true).setAutoWidth(true);

        procedureLayout.setWidth("100%");
        procedureLayout.setHeight("50%");
        procedureLayout.setAlignItems(Alignment.CENTER);

        procedureLayout.add(h1);
        procedureLayout.add(procedureGrid);
        procedureLayout.add(deleteButton);
    }
}
