package hu.itsec2.emszhw.ui.Pages.Insert;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import hu.itsec2.emszhw.model.Student;
import hu.itsec2.emszhw.services.User1Services;
import hu.itsec2.emszhw.services.User4Services;
import hu.itsec2.emszhw.ui.CustomComponents.Grids.Student1Grid;
import hu.itsec2.emszhw.ui.CustomComponents.Grids.Student4Grid;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.util.List;

@Route("insert")
public class InsertView extends VerticalLayout {
    private static final String DEFAULT_PASSWORD = "changeit";

    private final User1Services user1Services;
    private final User4Services user4Services;

    private final VerticalLayout insertLayout = new VerticalLayout();
    private final VerticalLayout procedureLayout = new VerticalLayout();

    private final FormLayout insertForm = new FormLayout();
    private final FormLayout procedureForm = new FormLayout();

    private final Button sqlButton = new Button("Submit");
    private final Button procedureButton = new Button("Submit");

    private final Student1Grid insertGrid = new Student1Grid();
    private final Student4Grid procedureGrid = new Student4Grid();

    public InsertView(User1Services user1Services, User4Services user4Services) {
        this.user1Services = user1Services;
        this.user4Services = user4Services;

        refreshGridData(insertGrid, user1Services.getStudentsData());
        refreshGridData(procedureGrid, user4Services.getStudentsData());

        setInsertLayout();
        setProcedureLayout();

        this.setHeight("200%");

        this.add(insertLayout, procedureLayout);
    }

    private void refreshGridData(Grid<Student> grid, List<Student> studentsData) {
        grid.setDataProvider(DataProvider.ofCollection(studentsData));
    }

    @PostConstruct
    public void afterConstruct() {
    }

    private void setInsertLayout() {
        TextField neptunCodeField = new TextField("Neptun code");
        TextField firstNameField = new TextField("First name");
        TextField lastNameField = new TextField("Last name");
        TextField motherNameField = new TextField("Mother's name");
        TextField emailAddressField = new TextField("Email address");
        TextField phoneNumberField = new TextField("Phone number");
        TextField nationalityField = new TextField("Nationality");
        TextField passwordField = new TextField("Password");
        Checkbox hasFailedInMathCheckbox = new Checkbox("Has failed in math");
        IntegerField yearOfEndField = new IntegerField("Years of end");

        insertForm.add(neptunCodeField);
        insertForm.add(lastNameField);
        insertForm.add(firstNameField);
        insertForm.add(motherNameField);
        insertForm.add(emailAddressField);
        insertForm.add(phoneNumberField);
        insertForm.add(nationalityField);
        insertForm.add(passwordField);
        insertForm.add(hasFailedInMathCheckbox);
        insertForm.add(yearOfEndField);
        insertForm.setWidth("50%");

        H1 h1 = new H1("Insert by SQL");
        h1.getElement().getStyle().set("margin-top", "10px");
        h1.getElement().getStyle().set("margin-bottom", "10px");


        sqlButton.addClickListener(click -> {
            Student student = new Student();
            student.setNeptunCode(neptunCodeField.getValue());
            student.setFirstName(firstNameField.getValue());
            student.setLastName(lastNameField.getValue());
            student.setMotherName(motherNameField.getValue());
            student.setEmailAddress(emailAddressField.getValue());
            student.setPhoneNumber(phoneNumberField.getValue());
            student.setNationality(nationalityField.getValue());
            student.setPassword(passwordField.getValue());
            student.setHasFailedInMath(hasFailedInMathCheckbox.getValue());
            student.setYearOfEnd(yearOfEndField.getValue());

            if (user1Services.insertNewStudent(student)) {
                Notification.show("Successful");
                refreshGridData(insertGrid, user1Services.getStudentsData());
            } else {
                Notification.show("Failed");
            }

            insertForm.getChildren().peek(element -> {
                if (element instanceof TextField) {
                    ((TextField) element).setValue("");
                } else if (element instanceof Checkbox) {
                    ((Checkbox) element).setValue(false);
                }
            }).close();
        });

        insertLayout.setWidth("100%");
        insertLayout.setHeight("50%");
        insertLayout.setAlignItems(Alignment.CENTER);
        insertLayout.setHorizontalComponentAlignment(Alignment.CENTER, insertForm);

        insertLayout.add(h1);
        insertLayout.add(insertForm);
        insertLayout.add(sqlButton);
        insertLayout.add(insertGrid);
    }

    private void setProcedureLayout() {
        TextField neptunCodeField = new TextField("Neptun code");
        TextField firstNameField = new TextField("First name");
        TextField lastNameField = new TextField("Last name");
        TextField motherNameField = new TextField("Mother's name");
        TextField emailAddressField = new TextField("Email address");
        TextField phoneNumberField = new TextField("Phone number");
        TextField nationalityField = new TextField("Nationality");
        Checkbox hasFailedInMathCheckbox = new Checkbox("Has failed in math");
        IntegerField yearOfEndField = new IntegerField("Years of end");

        procedureForm.add(neptunCodeField);
        procedureForm.add(lastNameField);
        procedureForm.add(firstNameField);
        procedureForm.add(motherNameField);
        procedureForm.add(emailAddressField);
        procedureForm.add(phoneNumberField);
        procedureForm.add(nationalityField);
        procedureForm.add(yearOfEndField);
        procedureForm.add(hasFailedInMathCheckbox);
        procedureForm.setWidth("50%");

        H1 h1 = new H1("Insert by procedure");
        h1.getElement().getStyle().set("margin-top", "10px");
        h1.getElement().getStyle().set("margin-bottom", "10px");

        procedureButton.addClickListener(click -> {
            Student student = new Student();
            student.setNeptunCode(neptunCodeField.getValue());
            student.setFirstName(firstNameField.getValue());
            student.setLastName(lastNameField.getValue());
            student.setMotherName(motherNameField.getValue());
            student.setEmailAddress(emailAddressField.getValue());
            student.setPhoneNumber(phoneNumberField.getValue());
            student.setNationality(nationalityField.getValue());
            student.setPassword(DEFAULT_PASSWORD);
            student.setHasFailedInMath(hasFailedInMathCheckbox.getValue());
            student.setYearOfEnd(yearOfEndField.getValue());

            try {
                if (user4Services.insertNewStudent(student)) {
                    Notification.show("Successful");
                    refreshGridData(procedureGrid, user4Services.getStudentsData());
                }
            } catch (SQLException e) {
                Notification.show(e.getMessage());
            }

            insertForm.getChildren().peek(element -> {
                if (element instanceof TextField) {
                    ((TextField) element).setValue("");
                } else if (element instanceof Checkbox) {
                    ((Checkbox) element).setValue(false);
                }
            }).close();
        });

        procedureLayout.setWidth("100%");
        procedureLayout.setHeight("50%");
        procedureLayout.setAlignItems(Alignment.CENTER);
        procedureLayout.setHorizontalComponentAlignment(Alignment.CENTER, procedureForm);

        procedureLayout.add(h1);
        procedureLayout.add(procedureForm);
        procedureLayout.add(procedureButton);
        procedureLayout.add(procedureGrid);
    }
}
