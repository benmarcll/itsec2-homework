package hu.itsec2.emszhw.ui.Pages.Treasure;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinSession;
import hu.itsec2.emszhw.utils.HtmlFileLoader;
import org.springframework.http.HttpStatus;

import java.io.IOException;

@Route("treasure")
public class TreasureView extends HorizontalLayout {

    public TreasureView() {

        if (VaadinSession.getCurrent().getAttribute("loggedInUser") != null) {
            this.setWidthFull();
            this.getElement().setProperty("innerHTML", HtmlFileLoader.getInstance().getHtmlContent("treasure.html"));
        } else {
            try {
                VaadinResponse.getCurrent().sendError(HttpStatus.UNAUTHORIZED.value(), "Authorization fail");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }

    }
}
