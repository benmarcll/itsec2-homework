package hu.itsec2.emszhw.ui.Pages.Login;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import lombok.Getter;

@Getter
public class HeaderComponent extends HorizontalLayout {

    private final HorizontalLayout tabsWrapper = new HorizontalLayout();

    private final Tabs tabs = new Tabs();

    private final Tab login1Tab = new Tab("Login1");
    private final Tab login2Tab = new Tab("Login2");
    private final Tab login3Tab = new Tab("Login3");
    private final Tab login4Tab = new Tab("Login4");


    public HeaderComponent() {
        this.setWidthFull();
        this.setDefaultVerticalComponentAlignment(Alignment.CENTER);

        setUpTabs();
        setUpTabsWrapper();

        this.add(tabsWrapper);
    }

    private void setUpTabs() {
        tabs.add(login1Tab, login2Tab, login3Tab, login4Tab);
    }

    private void setUpTabsWrapper() {
        tabsWrapper.setWidthFull();
        tabsWrapper.setJustifyContentMode(JustifyContentMode.CENTER);
        tabsWrapper.add(tabs);
    }

}
