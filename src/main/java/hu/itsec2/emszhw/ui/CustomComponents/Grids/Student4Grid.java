package hu.itsec2.emszhw.ui.CustomComponents.Grids;

import com.vaadin.flow.component.grid.Grid;
import hu.itsec2.emszhw.model.Student;

public class Student4Grid extends Grid<Student> {
    public Student4Grid() {
        this.addColumn(Student::getNeptunCode).setHeader("Neptun code").setSortable(true).setAutoWidth(true);
        this.addColumn(Student::getLastName).setHeader("Last name").setSortable(true).setAutoWidth(true);
        this.addColumn(Student::getFirstName).setHeader("First name").setSortable(true).setAutoWidth(true);
        this.addColumn(Student::getMotherName).setHeader("Mother's name").setSortable(true).setAutoWidth(true);
        this.addColumn(Student::getEmailAddress).setHeader("Email address").setSortable(true).setAutoWidth(true);
        this.addColumn(Student::getPhoneNumber).setHeader("Phone number").setSortable(true).setAutoWidth(true);
        this.addColumn(Student::getNationality).setHeader("Nationality").setSortable(true).setAutoWidth(true);
    }
}
