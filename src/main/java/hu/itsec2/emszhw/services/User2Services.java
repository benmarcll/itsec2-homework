package hu.itsec2.emszhw.services;

import hu.itsec2.emszhw.exception.PossibleSqlInjectionException;
import hu.itsec2.emszhw.model.User;
import hu.itsec2.emszhw.repository.User2Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional("db2TransactionManager")
public class User2Services {
    private final User2Repository user2Repository;

    public User2Services(User2Repository user2Repository) {
        this.user2Repository = user2Repository;
    }

    public User logIn(String username, String password) {

        List<User> loginResult = user2Repository.logIn(username, password);

        // this way it checks if the list contained more than one element (which is impossible for a valid login)
        if (loginResult.size() > 1) {
            throw new PossibleSqlInjectionException();
        } else if (loginResult.size() == 0) {
            return null;
        }

        return loginResult.stream().findFirst().get();


    }
}
