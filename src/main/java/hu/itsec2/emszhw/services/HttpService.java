package hu.itsec2.emszhw.services;

import hu.itsec2.emszhw.model.User;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

public class HttpService {

    private final WebClient webClient = WebClient.create();

    public String getLogin1 = webClient
            .get()
            .uri("https://localhost/api/login1")
            .retrieve()
            .bodyToMono(String.class)
            .block();

    public String getLogin2 = webClient
            .get()
            .uri("https://localhost/api/login2")
            .retrieve()
            .bodyToMono(String.class)
            .block();


    public void postVaadin(User loggedInUser) {
        try {
            if (loggedInUser != null) {
                webClient.post()
                        .uri("https://localhost/login")
                        .body(BodyInserters.fromValue(loggedInUser))
                        .retrieve()
                        .toEntity(String.class)
                        .block();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
