package hu.itsec2.emszhw.services;

import hu.itsec2.emszhw.exception.PossibleSqlInjectionException;
import hu.itsec2.emszhw.model.Student;
import hu.itsec2.emszhw.model.User;
import hu.itsec2.emszhw.repository.User4Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

@Service
@Transactional("db4TransactionManager")
public class User4Services {
    private final User4Repository user4Repository;

    public User4Services(User4Repository user4Repository) {
        this.user4Repository = user4Repository;
    }

    public User logIn(String username, String password) {

        List<User> loginResult = user4Repository.logIn(username, password);

        // this way it checks if the list contained more than one element (which is impossible for a valid login)
        if (loginResult.size() > 1) {
            throw new PossibleSqlInjectionException();
        } else if (loginResult.size() == 0) {
            return null;
        }

        return loginResult.stream().findFirst().get();


    }

    public List<Student> getStudentsData() {
        return user4Repository.getStudentData();
    }

    public Boolean deleteGraduatedStudents() {
        return user4Repository.deleteGraduatedStudents();
    }

    public boolean insertNewStudent(Student student) throws SQLException {
        return user4Repository.insertNewStudent(student);
    }
}
