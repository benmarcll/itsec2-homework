package hu.itsec2.emszhw.services;

import hu.itsec2.emszhw.model.Student;
import hu.itsec2.emszhw.model.User;
import hu.itsec2.emszhw.repository.User1Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional("db1TransactionManager")
public class User1Services {
    private final User1Repository user1Repository;

    public User1Services(User1Repository user1Repository) {
        this.user1Repository = user1Repository;
    }

    public User logIn(String username, String password) {

        // This way its no check that its originally wasnt a list
        return user1Repository.logIn(username, password).stream().findFirst().orElse(null);

    }

    public List<Student> getStudentsData() {
        return user1Repository.getStudentData();
    }

    public Boolean deleteStudents(Set<String> neptunCodes) {
        return user1Repository.deleteStudents(neptunCodes);
    }


    public Boolean insertNewStudent(Student student) {
        return user1Repository.insertNewStudent(student);
    }
}
