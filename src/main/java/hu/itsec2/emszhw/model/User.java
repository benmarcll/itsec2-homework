package hu.itsec2.emszhw.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    public User(String username) {
        this.username = username;
    }

    private String username;
    private String password;
}
