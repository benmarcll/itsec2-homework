package hu.itsec2.emszhw.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String neptunCode;
    private String firstName;
    private String lastName;
    private String motherName;
    private String emailAddress;
    private String phoneNumber;
    private String nationality;
    private String password;
    private Boolean hasFailedInMath;
    private Integer yearOfEnd;

    public Student(String neptunCode, String firstName, String lastName, String motherName, String emailAddress, String phoneNumber, String nationality, Integer yearOfEnd) {
        this.neptunCode = neptunCode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.motherName = motherName;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.nationality = nationality;
        this.yearOfEnd = yearOfEnd;
    }

    public String getHasFailedInMathHungarian() {
        return hasFailedInMath ? "Igaz" : "Hamis";
    }
}
