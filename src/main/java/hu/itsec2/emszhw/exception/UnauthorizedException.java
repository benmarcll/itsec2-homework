package hu.itsec2.emszhw.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


// Just a simple exception class, which purpose is to give 401 error code, which will be automatically mapped to 401.html
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Bad username or password")
public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException() {
        super();
    }
}

