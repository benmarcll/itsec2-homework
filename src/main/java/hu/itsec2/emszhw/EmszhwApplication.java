package hu.itsec2.emszhw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmszhwApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmszhwApplication.class, args);
	}

}
