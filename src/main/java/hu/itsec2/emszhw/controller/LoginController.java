package hu.itsec2.emszhw.controller;


import hu.itsec2.emszhw.utils.HtmlFileLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class LoginController {

    @GetMapping("login1")
    public String getLogin1() {
        return HtmlFileLoader.getInstance().getHtmlContent("login1.html");
    }

    @GetMapping("login2")
    public String getLogin2() {
        return HtmlFileLoader.getInstance().getHtmlContent("login2.html");
    }

}
