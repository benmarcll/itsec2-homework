package hu.itsec2.emszhw.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class HtmlFileLoader {
    private static final HtmlFileLoader INSTANCE = new HtmlFileLoader();

    private HtmlFileLoader() {
    }

    ;

    public static HtmlFileLoader getInstance() {
        return INSTANCE;
    }

    public String getHtmlContent(String htmlFileName) {
        try {
            return new String(Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream("static/" + htmlFileName)).readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return "no_html_found";
        }
    }
}
