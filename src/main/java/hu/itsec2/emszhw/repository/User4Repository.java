package hu.itsec2.emszhw.repository;

import hu.itsec2.emszhw.exception.UnauthorizedException;
import hu.itsec2.emszhw.model.Student;
import hu.itsec2.emszhw.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional("db4TransactionManager")
public class User4Repository {

    @Autowired
    @Qualifier("user4JdbcTemplate")
    private NamedParameterJdbcTemplate user4JdbcTemplate;

    private SimpleJdbcCall getSimpleJdbcCall() {
        return new SimpleJdbcCall(user4JdbcTemplate.getJdbcTemplate());
    }

    public List<User> logIn(String username, String password) {
        // Run the sql and get back the logged in user(s)
        try {
            return user4JdbcTemplate.query(
                    "select username, password from itsec2_admin.user4 where username = :username",
                    new MapSqlParameterSource("username", username),
                    (rs, rowNum) -> {
                        if (BCrypt.checkpw(password, rs.getString("password"))) {
                            return new User(
                                    rs.getString("username")
                            );
                        } else {
                            throw new UnauthorizedException();
                        }
                    }
            );
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Student> getStudentData() {

        return user4JdbcTemplate.query(
                "select * from itsec2_admin.student_infos",
                new MapSqlParameterSource(),
                (rs, rowNum) -> new Student(
                        rs.getString("neptun_code"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("mother_name"),
                        rs.getString("email_address"),
                        rs.getString("phone_number"),
                        rs.getString("nationality"),
                        rs.getInt("year_of_end")
                )
        );
    }

    public Boolean deleteGraduatedStudents() {
        try {
            getSimpleJdbcCall()
                    .withSchemaName("itsec2_admin")
                    .withProcedureName("del_graduated_students")
                    .execute();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean insertNewStudent(Student student) throws SQLException {
        try {
            getSimpleJdbcCall()
                    .withSchemaName("itsec2_admin")
                    .withProcedureName("add_new_student")
                    .execute(new MapSqlParameterSource()
                            .addValue("p_neptun_code", student.getNeptunCode())
                            .addValue("p_first_name", student.getFirstName())
                            .addValue("p_last_name", student.getLastName())
                            .addValue("p_mother_name", student.getMotherName())
                            .addValue("p_email_address", student.getEmailAddress())
                            .addValue("p_phone_number", student.getPhoneNumber())
                            .addValue("p_nationality", student.getNationality())
                            .addValue("p_has_failed_in_math", student.getHasFailedInMath() ? 1 : 0)
                            .addValue("p_password", BCrypt.hashpw(student.getPassword(), BCrypt.gensalt()))
                            .addValue("p_year_of_end", student.getYearOfEnd())
                    );
        } catch (UncategorizedSQLException e) {
            switch (e.getSQLException().getErrorCode()) {
                case 20000 -> throw new SQLException("Bad neptun code format");
                case 20001 -> throw new SQLException("Bad email format");
                case 20002 -> throw new SQLException("Bad phone number format");
                case 20003 -> throw new SQLException("Has failed in math value error");
                case 20004 -> throw new SQLException("Year of end value error");
                case 20005 -> throw new SQLException("Some of the fields are empty");
                case 20006 -> throw new SQLException("Neptun code is not unique");
                case 20007 -> throw new SQLException("Email is not unique");
                case 20008 -> throw new SQLException("Phone number is not unique");
            }
        }

        return true;
    }
}
