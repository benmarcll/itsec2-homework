package hu.itsec2.emszhw.repository;

import hu.itsec2.emszhw.model.Student;
import hu.itsec2.emszhw.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
@Transactional("db1TransactionManager")
public class User1Repository {

    @Autowired
    @Qualifier("itsec2AdminJdbcTemplate")
    private NamedParameterJdbcTemplate itsec2AdminJdbcTemplate;

    @Autowired
    @Qualifier("user1JdbcTemplate")
    private NamedParameterJdbcTemplate user1JdbcTemplate;


    public List<User> logIn(String username, String password) {

        // Raped Spring to make the login sql injectioned
        String sql = "select username,password from user1 where username = ':username' and password = ':password'"
                .replace(":username", username).replace(":password", password);

        // Run the sql and get back the logged in user(s)
        try {
            return itsec2AdminJdbcTemplate.query(
                    sql,
                    (rs, rowNum) ->
                            new User(
                                    rs.getString("username"),
                                    rs.getString("password")
                            )

            );
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Student> getStudentData() {

        return user1JdbcTemplate.query(
                "select * from itsec2_admin.student1",
                new MapSqlParameterSource(),
                (rs, rowNum) ->
                        new Student(
                                rs.getString("neptun_code"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("mother_name"),
                                rs.getString("email_address"),
                                rs.getString("phone_number"),
                                rs.getString("nationality"),
                                rs.getString("password"),
                                rs.getInt("has_failed_in_math") == 1 ? true : false,
                                rs.getInt("year_of_end")
                        )

        );
    }

    public Boolean deleteStudents(Set<String> neptunCodes) {
        if (!neptunCodes.isEmpty()) {
            return user1JdbcTemplate.update(
                    "delete from itsec2_admin.student1 where neptun_code in (:neptun_codes)",
                    new MapSqlParameterSource("neptun_codes", neptunCodes)
            ) > 0;
        } else return false;
    }

    public Boolean insertNewStudent(Student student) {
        try {
            return user1JdbcTemplate.update("insert into itsec2_admin.student1(neptun_code,first_name,last_name,mother_name,email_address,phone_number,nationality,password,has_failed_in_math,year_of_end) values(:neptun_code,:first_name,:last_name,:mother_name,:email_address,:phone_number,:nationality,:password,:has_failed_in_math,:year_of_end)",
                    new MapSqlParameterSource(
                            "neptun_code", student.getNeptunCode()).addValue(
                            "first_name", student.getFirstName()).addValue(
                            "last_name", student.getLastName()).addValue(
                            "mother_name", student.getMotherName()).addValue(
                            "email_address", student.getEmailAddress()).addValue(
                            "phone_number", student.getPhoneNumber()).addValue(
                            "nationality", student.getNationality()).addValue(
                            "password", BCrypt.hashpw(student.getPassword(), BCrypt.gensalt())).addValue(
                            "has_failed_in_math", student.getHasFailedInMath()).addValue(
                            "year_of_end", student.getYearOfEnd())
            ) == 1;
        } catch (Exception e) {
            return false;
        }
    }
}
