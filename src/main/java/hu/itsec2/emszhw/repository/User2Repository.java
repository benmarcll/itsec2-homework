package hu.itsec2.emszhw.repository;

import hu.itsec2.emszhw.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional("db2TransactionManager")
public class User2Repository {

    @Autowired
    @Qualifier("user2JdbcTemplate")
    private NamedParameterJdbcTemplate user2JdbcTemplate;

    public List<User> logIn(String username, String password) {
        // Run the sql and get back the logged in user(s)
        try {
            return user2JdbcTemplate.query(
                    "select username, password from itsec2_admin.user2 where username = :username and password = :password",
                    new MapSqlParameterSource("username", username).addValue("password", password),
                    (rs, rowNum) ->
                            new User(
                                    rs.getString("username"),
                                    rs.getString("password")
                            )

            );
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

}
