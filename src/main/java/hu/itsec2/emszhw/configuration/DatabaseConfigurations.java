package hu.itsec2.emszhw.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class DatabaseConfigurations {

    @Bean(name = "dbAdmin")
    @ConfigurationProperties(prefix = "spring.itsec2admin.datasource")
    public DataSource dataSourceAdmin() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "itsec2AdminJdbcTemplate")
    public NamedParameterJdbcTemplate jdbcTemplateAdmin(@Qualifier("dbAdmin") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean(name = "dbAdminTransactionManager")
    public PlatformTransactionManager dbAdminTransactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSourceAdmin());
        return transactionManager;
    }

    @Bean(name = "db1")
    @ConfigurationProperties(prefix = "spring.user1.datasource")
    public DataSource dataSource1() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "user1JdbcTemplate")
    public NamedParameterJdbcTemplate jdbcTemplate1(@Qualifier("db1") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean(name = "db1TransactionManager")
    public PlatformTransactionManager db1TransactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource1());
        return transactionManager;
    }

    @Bean(name = "db2")
    @ConfigurationProperties(prefix = "spring.user2.datasource")
    public DataSource dataSource2() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "user2JdbcTemplate")
    public NamedParameterJdbcTemplate jdbcTemplate2(@Qualifier("db2") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean(name = "db2TransactionManager")
    public PlatformTransactionManager db2TransactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource2());
        return transactionManager;
    }

    @Bean(name = "db3")
    @ConfigurationProperties(prefix = "spring.user3.datasource")
    public DataSource dataSource3() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "user3JdbcTemplate")
    public NamedParameterJdbcTemplate jdbcTemplate3(@Qualifier("db3") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean(name = "db3TransactionManager")
    public PlatformTransactionManager db3TransactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource3());
        return transactionManager;
    }

    @Bean(name = "db4")
    @ConfigurationProperties(prefix = "spring.user4.datasource")
    public DataSource dataSource4() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "user4JdbcTemplate")
    public NamedParameterJdbcTemplate jdbcTemplate4(@Qualifier("db4") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean(name = "db4TransactionManager")
    public PlatformTransactionManager db4TransactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource4());
        return transactionManager;
    }

}